Chat server with cross-platform clients and fun animated faces, written in Haxe.


# Features

* Cross-platform clients, including desktop (Windows/Mac/Linux), Android, and 
Flash. Android APK and executables for Linux and Windows are available as 
downloads on BitBucket. (Unfortunately I don't have a Mac to build on at the 
moment.) The clients are all interoperable: you can use the Flash client to chat 
with you on your Android phone, etc.

* Animated faces: in the graphical client, you'll have a customizable face, and 
your facial expressions change when you use smileys while talking (try ":)" or 
":(".) The server still works via telnet - users logged on without a graphical 
client just won't see any of the faces or have a face themselves.

* Private messages: "/m Ben this is such a great server! wow! so impressed! :D"


# Instructions

## Run the clients

* At the login screen, enter your name and click or tap any of the face options 
to change them.

* On Android, press the Menu button at any time to bring up the virtual 
keyboard.

* Once logged in, the following command are available:

    - Private message: /m name your message here
    - Change rooms: /join different_room
    - View all rooms: /rooms
    - Set status to away: /brb
    - Return from away: /back
    - Quit: /q


## Run a MoChat server

If you'd like to run the server on your own machine, you can install Haxe 3.0 
and the Neko VM 2.0.0 and run "make server" (will require GNU Make to be 
installed.) If you use Homebrew, Haxe and Neko can both be installed by running 
"brew install haxe." Otherwise, you can get Haxe here: 
<http://haxe.org/download> and Neko here: <http://nekovm.org/download>.

Alternatively, the Neko bytecode is included as a download on BitBucket, so you 
can install just Neko (<http://nekovm.org/download>) and run the server with 
"neko server.n."

The server can be run with a custom host/port like this: "neko server.n 
127.0.0.1 27278"

## Compile the clients

The desktop targets are a little more complicated, as they will require 
installing openfl, openfl-native, and lime, and building lime for your platform.
