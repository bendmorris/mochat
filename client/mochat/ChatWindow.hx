package mochat;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.graphics.BitmapText;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import mochat.Face;

using StringTools;


class ChatWindow extends Scene {
    var faces:Map<String, FaceEntity>;
    var client:Client;
    var name:String;
    var console:BitmapText;
    var input:BitmapText;
    
    public function new(name:String, face:FaceEntity) {
        this.name = name;
        faces = new Map();
        faces[name] = face;
        Input.define("enter", [Key.ENTER, 10]);
        Input.define("menu", [16777234]);
        Input.textKeys = Key.validInputChars;
        Input.keyString = "";
        Input.kKeyStringMax = 140;
        super();
    }
    
    override public function begin() {
        client = new Client(name, faces[name]);
        add(faces[name]);
        
        console = new BitmapText("",Defs.WIDTH/2,0,
                                    Defs.WIDTH/2-Std.int(10*Defs.SCALE),
                                    Defs.HEIGHT*2,
                                    Defs.FONT_OPTIONS);
        var consoleEntity = new Entity(0,0,console);
        add(consoleEntity);
        
        input = new BitmapText(">>> ",Defs.WIDTH/2,Defs.HEIGHT*3/4,
                                      Defs.WIDTH/2-Std.int(10*Defs.SCALE),
                                      Defs.HEIGHT,
                                      Defs.FONT_OPTIONS);
        var inputEntity = new Entity(0,0,input);
        add(inputEntity);
        
        HXP.stage.requestSoftKeyboard();
    }
    
    override public function update() {
        if (Input.pressed("menu") || Input.mousePressed) HXP.stage.requestSoftKeyboard();
        
        client.update();
        
        if (client.disconnected) {
            console.text = "Your connection has been lost.";
        } else {
            // update console with new text received from server
            var newMsgs:String = "";
            while (client.messageQueue.length > 0) {
                var msg = client.messageQueue.pop();
                if (msg.startsWith(">>> ")) {
                    // chat message
                    var sender = msg.substr(4).split(":")[0];
                    var msg = msg.split(":").slice(1).join(":").trim();
                    var words = msg.split(" ");
                    if (faces.exists(sender)) {
                        faces[sender].face.speak(words);
                    }
                }
                newMsgs += (newMsgs == "" ? "" : "\n") + msg;
            }
            
            if (client.removeAllFaces) {
                for (x in faces.keys()) {
                    if (x != name) {
                        removeFace(x);
                    }
                }
                client.removeAllFaces = false;
            }
            
            while (client.newFaces.length > 0) {
                var face = client.newFaces.pop();
                var faceEntity = new FaceEntity();
                faces[face.name] = faceEntity;
                faceEntity.skin = face.skin;
                faceEntity.hair = face.hair;
                faceEntity.randomLocation();
                add(faceEntity);
            }
            while (client.leave.length > 0) {
                // someone left the room
                var x = client.leave.pop();
                removeFace(x);
            }
            if (newMsgs != "") {
                console.text += newMsgs;
            }
            if (console.textHeight > console.height) {
                console.textHeight = 0;
                console.text = console.lines.slice(1).join("\n");
            }
            console.y = Defs.HEIGHT*3/4 - console.textHeight;
            
            // text input field
            input.text = ">>> " + Input.keyString;
            
            if (Input.pressed("enter")) {
                // send a message to the server
                client.sendMessage(Input.keyString);
                Input.keyString = "";
            }
        }
        
        super.update();
    }
    
    function removeFace(name:String) {
        if (faces.exists(name)) {
            remove(faces[name]);
            faces.remove(name);
        }
    }
}
