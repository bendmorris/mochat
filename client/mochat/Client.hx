package mochat;

import haxe.io.Bytes;
import haxe.Serializer;
import haxe.Unserializer;
import flash.net.Socket;
import flash.events.Event;
import flash.events.DataEvent;
import com.haxepunk.HXP;
import mochat.Defs;
import mochat.Face;
import mochat.FaceInfo;


class Client {
    public var socket:Socket;
    public var id:Guid;
    public var messageQueue:List<String>;
    public var newFaces:List<FaceInfo>;
    public var leave:List<String>;
    public var face:FaceEntity;
    public var connected:Bool=false;
    public var disconnected:Bool=false;
    public var removeAllFaces:Bool=false;
    
    var name:String;
    
    var echo:Bool=false;
    
    public function new(name:String, face:FaceEntity) {
        this.name = name;
        this.face = face;
        messageQueue = new List();
        newFaces = new List();
        leave = new List();
        connect();
    }
    
    public function connect() {
#if flash
        flash.system.Security.loadPolicyFile("http://" + Defs.HOST + "/crossdomain.xml");
#end
        socket = new Socket();
        socket.addEventListener(Event.CONNECT, onConnect);
        var host = Defs.HOST;
        var port = Defs.PORT;
        socket.connect(host, port);
    }
    
    function onConnect(event:Event) {
        connected = true;
        Data.sendData(socket, name);
        echo = true;
    }
    
    public function sendMessage(msg:String) {
        if (disconnected) return;
        Data.sendData(socket, msg);
    }
    
    function disconnect() {
        disconnected = true;
    }
    
    public function update() {
        if (connected && !socket.connected) disconnect();
        
        if (disconnected) return;
        
        while (socket.bytesAvailable > 0) {
            var msg = Data.readData(socket);
            if (echo) {
                for (m in msg.split("\n")) {
                    // process message
                    m = StringTools.trim(m);
                    if (m == "# Hi, " + name + "!") {
                        // welcome message, send info about your face to the server
                        Data.sendData(socket, "/face " + face.skin + "," + face.hair);
                        messageQueue.add(m);
                        
                    } else if (StringTools.startsWith(m, "# /face")) {
                        // got info about someone else's face
                        var msg = m.substr(8);
                        var parts = msg.split(" ");
                        var name = parts[0];
                        var attributes = parts[1].split(",");
                        var skin = attributes[0];
                        var hair = attributes[1];
                        newFaces.push({name:name, skin:skin, hair:hair});
                        
                    } else if (m == "# Users in this room:") {
                        // new room; wipe all previous faces
                        removeAllFaces = true;
                        messageQueue.add(m);
                        
                    } else if (StringTools.startsWith(m, "# ") && 
                               StringTools.endsWith(m, " has left.")) {
                        // someone left; remove their face
                        var name = m.substr(2).split(" ")[0];
                        leave.push(name);
                        messageQueue.add(m);
                        
                    } else {
                        // any other message
                        messageQueue.add(m);
                    }
                }
            }
        }
    }
}
