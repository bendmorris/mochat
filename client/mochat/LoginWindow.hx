package mochat;

import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.Entity;
import com.haxepunk.graphics.BitmapText;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import mochat.Face;


enum AvatarOption {
    Name;
    Skin;
    HairStyle;
    HairColor;
}

class LoginWindow extends Scene {
    static var avatarOptions = [Name, Skin, HairStyle, HairColor];
    static var optionNames = [
        Name => "Name",
        Skin => "Skin color",
        HairStyle => "Hairstyle",
        HairColor => "Hair color",
    ];
    static var optionChoices = [
        Skin => ["pale", "tan", "dark", "red", "green", "blue", "purple", "white"],
        HairStyle => ["(NONE)", "1", "2", "3", "4"],
        HairColor => ["brown", "blonde", "black", "red", "white"],
    ];
    
    var selectedOption = 0;
    var labels:Array<Entity>;
    var selected:Array<String>;
    var face:FaceEntity;
    
    public function new() {
        Input.define("enter", [Key.ENTER, 10]);
        Input.define("menu", [16777234]);
        Input.textKeys = [Key.HYPHEN];
        super();
    }
    
    override public function begin() {
        face = new FaceEntity(Defs.WIDTH/4, 0, "happy");
        face.y = Defs.HEIGHT/2 - face.height/2;
        add(face);
        
        // select the first option for each choice
        selected = [];
        selected.push("");
        for (x in avatarOptions.slice(1)) {
            selected.push(optionChoices[x][0]);
        }
        
        labels = [];
        var labelHeight = Defs.HEIGHT/(avatarOptions.length+2);
        var n = 0;
        for (option in avatarOptions) {
            var label = new BitmapText(optionNames[option] + ":",0,0,Defs.WIDTH/2,labelHeight,Defs.FONT_OPTIONS);
            labels.push(new Entity(Defs.WIDTH/2, (n+0.5)*labelHeight, label));
            add(labels[labels.length-1]);
            n += 1;
        }
        
        var label = new BitmapText((#if mobile "Touch" #else "Click" #end) + " here to log in!",0,0,Defs.WIDTH/2,labelHeight,Defs.FONT_OPTIONS);
        labels.push(new Entity(Defs.WIDTH/2, (n+0.5)*labelHeight, label));
        add(labels[labels.length-1]);
        
        HXP.stage.requestSoftKeyboard();
    }
    
    override public function update() {
        if (Input.pressed("menu")) HXP.stage.requestSoftKeyboard();
        
        if (Input.mousePressed) {
            // clicking on option fields changes them
            var mx = mouseX, my = mouseY;
            for (n in 0 ... labels.length) {
                var label = labels[n];
                if (label.collidePoint(label.x, label.y, mx, my)) {
                    if (n == 0) {
                        HXP.stage.requestSoftKeyboard();
                    } else if (n == labels.length - 1) {
                        login();
                    } else {
                        // advance to the next variation
                        var option = avatarOptions[n];
                        var choices = optionChoices[option];
                        var newIndex = (Lambda.indexOf(choices, selected[n]) + 1) % choices.length;
                        selected[n] = choices[newIndex];
                        
                        // update the face
                        switch(option) {
                            case Skin: {
                                face.skin = selected[n];
                            }
                            case HairStyle, HairColor: {
                                var hair:String = "";
                                var style = selected[Lambda.indexOf(avatarOptions, HairStyle)];
                                var color = selected[Lambda.indexOf(avatarOptions, HairColor)];
                                if (style == "(NONE)") {
                                    hair = "";
                                } else {
                                    hair = style + "_" + color;
                                }
                                face.hair = hair;
                            }
                            default: {
                            }
                        }
                    }
                }
            }
        }
        
        // enter login name
        Input.keyString = StringTools.replace(Input.keyString, ' ', '');
        selected[0] = Input.keyString.substr(-10,10);
        
        for (n in 0 ... labels.length) {
            // update labels and hitboxes
            var label = labels[n];
            var option = avatarOptions[n];
            var chosen = selected[n];
            var labelGraphic = cast(label.graphic, BitmapText);
            if (n < labels.length-1) {
                labelGraphic.text = optionNames[option] + ":\n  " + chosen;
            }
            label.setHitbox(labelGraphic.textWidth, labelGraphic.textHeight);
        }
        
        if (Input.pressed("enter")) login();
        
        super.update();
    }
    
    function login() {
        if (selected[0].length == 0) return;
        
        remove(face);
        face.face.skin = "normal";
        HXP.scene = new ChatWindow(selected[0], face);
    }
}
