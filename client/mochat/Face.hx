package mochat;

import spinehaxe.Bone;
import spinehaxe.Slot;
import spinehaxe.SkeletonData;
import spinehaxe.SkeletonJson;
import spinehaxe.animation.Animation;
import spinehaxe.animation.AnimationState;
import spinehaxe.animation.AnimationStateData;
import spinehaxe.animation.TrackEntry;
import spinehaxe.atlas.TextureAtlas;
import spinehaxe.attachments.AtlasAttachmentLoader;
import spinehaxe.attachments.Attachment;
import spinehaxe.attachments.RegionAttachment;
import spinehaxe.platform.nme.BitmapDataTextureLoader;
import spinepunk.SpinePunk;
import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import flash.display.BitmapData;
import flash.geom.Point;
import openfl.Assets;
import mochat.Defs;


class FaceEntity extends Entity {
    public var face:Face;
    
    public function new(x:Float=0, y:Float=0, skin:String="normal") {
        face = new Face(skin);
        // randomize talking/blinking speed slightly
        face.speed += Math.random() * 0.2;
        graphic = face;
        super(x,y);
    }
    
    override public function added() {
        collidable = true;
        type = "face";
    }
    
    override public function update() {
        face.update();
        setHitboxTo(face.mainHitbox);
        
        if (collide("face", x, y) != null) {
            randomLocation();
        }
        
        super.update();
    }
    
    public var skin(default, set):String="pale";
    function set_skin(skin:String) {
        face.setSpineAttachment("head", "face_head_" + skin);
        return this.skin = skin;
    }
    
    public var hair(default, set):String="";
    function set_hair(hair:String) {
        face.setSpineAttachment("hair", hair=="" ? "" : "face_hair_" + hair);
        return this.hair = hair;
    }
    
    public function randomLocation() {
        var n = 0;
        // place the face somewhere random, and try to keep it from
        // colliding with other faces if possible
        do {
            n += 1;
            x = Defs.WIDTH/6 + Math.random() * Defs.WIDTH/6;
            y = Defs.HEIGHT/4 + Math.random() * Defs.HEIGHT/2;
        } while (n < 10 && 
            collide("face", x, y) != null);
    }
}


class Face extends SpinePunk {
    public var loader:BitmapDataTextureLoader;
    public var atlas:TextureAtlas;
    
    var animationQueue:List<String>;
    
    public var animation:String;
    public function setAnimation(animationName:String, force=false) {
        // either set skin (new face) or start/stop talking
        
        switch(animationName) {
            case "happy", "sad", "normal": {
                skin = animationName;
                return;
            }
        }
        
        if (force || animation != animationName) {
            if (animationName != null) {
                this.state.setAnimationByName(0, animationName, (animationName == "blink") ? true : false);
            }
            animation = animationName;
        }
    }
    
    public function new(?skin:String) {
        loader = new BitmapDataTextureLoader();
        if (atlas == null) {
            atlas = TextureAtlas.create(Assets.getText("graphics/face.atlas"),
                                        "graphics/", loader);
        }
        
        var json = SkeletonJson.create(atlas);
        var skeletonData:SkeletonData = json.readSkeletonData(Assets.getText("graphics/face.json"), "face");
        
        super(skeletonData, true);
        
        if (skin != null) this.skin = skin;
        else this.skin = "normal";
        
        this.stateData = new AnimationStateData(skeletonData);
        this.state = new AnimationState(this.stateData);
        state.clearWhenFinished = false;
        
        setAnimation("blink");
        
        // set animation transitions
        this.stateData.defaultMix = 0.1;
        scale = Defs.SCALE*2;
        hitboxSlots=["head"];
        
        animationQueue = new List();
    }
    
    public function setSpineAttachment(slotName:String, attachmentName:String) {
        var slot = skeleton.findSlot(slotName);
        var data = slot.data;
        data.attachmentName = attachmentName;
        slot.setToSetupPose();
    }
    
    public var track(get, never):TrackEntry;
    function get_track() {
        var track = state.tracks[0];
        if (track != null) return track;
        return null;
    }
    public var time(get, never):Float;
    function get_time():Float {
        var anim = track;
        if (anim == null) return 0;
        return anim.time;
    }
    public var duration(get, never):Float;
    function get_duration():Float {
        var anim = track;
        if (anim == null) return 0;
        return anim.endTime;
    }
    public var remaining(get, never):Float;
    function get_remaining() {
        return animation == "blink" ? 1 : duration - time;
    }
    
    override public function update() {
        super.update();
        
        if (remaining <= 0) {
            if (animationQueue.length > 0) {
                setAnimation(animationQueue.pop(), true);
            } else {
                setAnimation("blink");
            }
        }
    }
    
    public function speak(words:Array<String>) {
        while (animationQueue.length > 0) animationQueue.pop();
        
        var firstEmotion = true;
        for (word in words) {
            var anim = switch(word) {
                case ":)", ":-)", ":D", ":-D": "happy";
                case ":(", ":-(": "sad";
                case ":|", ":-|": "normal";
                default: "talk";
            }
            
            if (anim != "talk" && firstEmotion) {
                firstEmotion = false;
                skin = anim;
            } else {
                animationQueue.add(anim);
            }
        }
        animationQueue.add("blink");
        
        setAnimation(animationQueue.pop(), true);
    }
}
