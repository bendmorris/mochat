import com.haxepunk.Engine;
import com.haxepunk.HXP;
import com.haxepunk.RenderMode;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.system.Capabilities;
import flash.geom.Point;
import flash.Lib;
import mochat.Defs;
import mochat.LoginWindow;


class Main extends Engine {
    public var windowWidth:Int=0;
    public var windowHeight:Int=0;
    
#if desktop
    private static var lime_stage_set_window_position = Lib.load ("lime", "lime_stage_set_window_position", 3);
#end
    
    public override function new(width:Int=0, height:Int=0, frameRate:Float=60, fixed:Bool=false, ?renderMode:RenderMode) {
#if desktop
        var resolution:Point = Defs.RESOLUTIONS.pop();
        while (resolution.x > Capabilities.screenResolutionX || 
               resolution.y > Capabilities.screenResolutionY) {
            resolution = Defs.RESOLUTIONS.pop();
            if (resolution == null) {
                throw ("Invalid screen resolution: " + Capabilities.screenResolutionX + "x" + Capabilities.screenResolutionY);
            }
        }
        
        Defs.SCALE *= resolution.y / Defs.HEIGHT;
        windowWidth = Defs.WIDTH = Std.int(resolution.x);
        windowHeight = Defs.HEIGHT = Std.int(resolution.y);
        
        super(Defs.WIDTH, Defs.HEIGHT, Defs.FPS, fixed, renderMode);
#elseif mobile
        super(width, height, Defs.FPS, fixed, renderMode);
        
        var s = HXP.stage.stageWidth / Defs.WIDTH;
        Defs.SCALE *= s;
        Defs.WIDTH = Std.int(Defs.WIDTH*s);
        Defs.HEIGHT = Std.int(Defs.HEIGHT*s);
        
        HXP.resize(Defs.WIDTH, Defs.HEIGHT);
#else
        super(width, height, Defs.FPS, fixed, renderMode);
#end
    }
    
    override public function init() {
        onResize();
        
#if desktop
        HXP.stage.addEventListener(Event.RESIZE, onResize); 
        HXP.stage.dispatchEvent(new Event(Event.RESIZE));
#end
        HXP.screen.color = Defs.BGCOLOR;
        
        HXP.scene = new LoginWindow();
    }
    
    public static function main() { new Main(); }
    
    public function onResize(event:Event=null) {
        HXP.screen.scaleX = HXP.screen.scaleY = Std.int(Math.round(HXP.screen.scaleY));
#if desktop
        centerWindow();
#end
#if mobile
#end
    }

    public override function onStage(e:Event = null) {
        super.onStage();
        
#if desktop
        HXP.stage.resize(Defs.WIDTH, Defs.HEIGHT);
        centerWindow();
#end
    }

#if desktop
    function centerWindow() {
        var sx = (Capabilities.screenResolutionX - Defs.WIDTH) / 2;
        var sy = (Capabilities.screenResolutionY - Defs.HEIGHT) / 2;
        lime_stage_set_window_position(HXP.stage.__handle, sx, sy);
    }
#end
}
