name=mochat

all:

.PHONY: all debug pkg \
        flash flash-debug flash-final \
        linux linux-debug linux-profile \
        windows windows-debug \
        mac mac-debug \
        android android-debug android-install \
		as3 \
		server

debug: flash-debug

pkg: android server
	mkdir -p pkg
	cp server.n pkg/mochat-server.n
	cp bin/android/bin/bin/mochat-release.apk pkg/mochat-client-android.apk


# targets

flash:
	haxelib run openfl build flash -v
	chromium flash.html
flash-debug:
	haxelib run openfl build flash -debug -v
	chromium flash.html
flash-final:
	haxelib run openfl build flash -Dfinal -v
	chromium flash.html

linux:
	haxelib run openfl test linux -Ddesktop -v
linux-debug:
	haxelib run openfl test linux -Ddesktop -v -debug
linux-final:
	haxelib run openfl test linux -v -Dfinal

windows:
	haxelib run openfl test windows -Ddesktop -v
windows-debug:
	haxelib run openfl test windows -Ddesktop -v -debug

mac:
	haxelib run openfl test mac -Ddesktop -v
mac-debug:
	haxelib run openfl test mac -Ddesktop -v -debug

android: android-keystore
	haxelib run openfl build android -v
android-debug:
	haxelib run openfl build android -v -debug
android-install: android
	adb uninstall com.ben.$(name)
	adb install bin/android/bin/bin/$(name)-release.apk
android-keystore: $(name).keystore
$(name).keystore:
	keytool -genkey -v -keystore "$(name).keystore" -alias $(name) -keyalg RSA -keysize 2048 -validity 10000

server:
	haxe -main mochat.Server -neko server.n -cp src -D server
	nekotools boot server.n
server-run: server
	./server


# dependencies

assets: scripts/inkscape_split.py $(wildcard assets/spine/*.spine) $(wildcard assets/spine/*.svg)
	for i in $(patsubst %.spine, %.svg, $(wildcard assets/spine/*.spine)); \
    do \
        python $< $$i; \
    done
	touch assets

