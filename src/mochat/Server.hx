package mochat;

import haxe.io.Bytes;
import haxe.Serializer;
import haxe.Unserializer;
import neko.net.ThreadServer;
import sys.net.Socket;
import mochat.ClientApi;
import mochat.Defs;
import mochat.FaceInfo;


typedef Message = String;
typedef Room = Map<Guid, ClientApi>;

class Server extends ThreadServer<ClientApi, Message> {
    var id:Guid;
    
    var clients:Map<Guid, ClientApi>;
    var faces:Map<Guid, FaceInfo>;
    var nameToId:Map<String, Guid>;
    var rooms:Map<String, Room>;
    
    function new() {
        super();
        id = Defs.guid();
        
        clients = new Map();
        faces = new Map();
        nameToId = new Map();
        rooms = new Map();
    }
    
    override function clientConnected(s:Socket) {
        var client = new ClientApi(s);
        trace("adding user (id=" + client.id + ")");
        clients[client.id] = client;
        client.loginPrompt();
        return client;
    }
    
    override function clientDisconnected(client:ClientApi) {
        trace("disconnect (id=" + client.id + ")");
        
        leaveRoom(client);
        
        if (client.name != null && nameToId.exists(client.name)) {
            nameToId.remove(client.name);
        }
        
        if (clients.exists(client.id)) {
            clients.remove(client.id);
        }
    }
    
    /*
     * Read a messages (Bytes) into a String for processing
     */
    override function readClientMessage(client:ClientApi, buf:Bytes, pos:Int, len:Int) {
        var msg:String = buf.readString(pos, len);
        return {msg: msg, bytes: len};
    }
    
    /*
     * Handle a message received from the client.
     */
    override function clientMessage(client:ClientApi, msg:Message) {
        msg = StringTools.trim(msg);
        trace(client.id + ": '" + msg + "' (" + msg.length + ")");
        
        if (msg.charCodeAt(0) == 60) {
            if (msg == "<policy-file-request/>\x00") {
            //if (msg.indexOf("<policy-file-request/>") > -1) {
                // flash policy file request
                var policyFile = '<?xml version="1.0"?>
<cross-domain-policy>
   <site-control permitted-cross-domain-policies="all"/>
   <allow-access-from domain="*" to-ports="' + Defs.PORT + '"/>
   <allow-http-request-headers-from domain="*" headers="*"/>
</cross-domain-policy>\x00';
                trace(policyFile);
                client.sendMessage(policyFile, false, false);
                return;
            }
        }
        
        if (msg == "") {
            return;
        }
        
        switch (client.state) {
            case LOGIN: {
                if (nameToId.exists(msg)) {
                    // this name is taken
                    client.sendMessage("Sorry, that name is taken. Please choose a different name.");
                } else if (msg.indexOf(" ") > -1) {
                    client.sendMessage("Your name can't contain spaces.");
                } else {
                    // logged in successfully
                    trace("login (id=" + client.id + ", name=" + msg + ")");
                    nameToId[msg] = client.id;
                    client.name = msg;
                    client.welcome();
                    joinRoom(client, "home");
                }
            }
            
            case CHAT, AWAY: {
                // check for special commands
                var firstWord = msg.split(" ")[0];
                switch(firstWord) {
                    case "/rooms": {
                        client.sendMessage("Available rooms:");
                        for (k in rooms.keys()) {
                            var thisRoom = rooms[k];
                            var n = 0;
                            for (i in thisRoom.keys()) { n += 1; }
                            client.sendMessage("* " + k + " (" + n + ")");
                        }
                    }
                    case "/face": {
                        // register face appearance
                        var words = msg.split(" ").slice(1)[0].split(",");
                        var skin = words[0];
                        var hair = words[1];
                        faces[client.id] = {name:client.name, skin:skin, hair:hair};
                        
                        sendFace(client);
                    }
                    case "/faces": {
                        // request faces of everyone in the room
                        //client.sendMessage("Faces:");
                    }
                    case "/join": {
                        // join a room
                        var newRoom = msg.split(" ").slice(1).join(" ");
                        joinRoom(client, newRoom);
                        
                        sendFace(client);
                    }
                    case "/away", "/brb": {
                        client.state = AWAY;
                        client.sendMessage("You're now away. Enter /back to come back.");
                    }
                    case "/back": {
                        client.state = CHAT;
                        client.sendMessage("Welcome back!");
                    }
                    case "/quit", "/q", "/exit": {
                        client.sendMessage("Bye!");
                        client.disconnect(this);
                        return;
                    }
                    case "/msg", "/m": {
                        if ((msg.split(" ")).length < 3) {
                            client.sendMessage("Usage: /m user_name Your message here");
                        } else {
                            var name = msg.split(" ")[1];
                            var msg = msg.split(" ").slice(2).join(" ");
                            if (nameToId.exists(name)) {
                                var userId = nameToId[name];
                                var target = clients[userId];
                                client.sendMessage("[To " + target.name + ": " + msg + "]");
                                target.sendMessage("[From " + client.name + ": " + msg + "]");
                            } else {
                                client.sendMessage("There isn't a user with named " + name + ".");
                            }
                        }
                    }
                    default: {
                        // no command, so chat
                        if (client.state == AWAY) {
                            client.sendMessage("You're still away. Enter /back to come back.");
                        } else {
                            chat(client, msg);
                        }
                    }
                }
            }
            default: {}
        }
    }
    
    function chat(client:ClientApi, msg:String) {
        broadcast(client, ">>> " + client.name + ": " + msg, true, false);
    }
    
    /*
     * Send a message to every other client in the same room.
     */
    function broadcast(client:ClientApi, msg:String, self=false, official=true) {
        var room = client.room;
        if (room == null) return;
        
        if (rooms.exists(room)) {
            var thisRoom = rooms[room];
            
            for (k in thisRoom.keys()) {
                var neighbor = thisRoom[k];
                if (self || neighbor != client) {
                    neighbor.sendMessage(msg, official);
                }
            }
        }
    }
    
    function sendFace(client:ClientApi) {
        var room = client.room;
        if (room == null) return;
        
        if (rooms.exists(room)) {
            var thisRoom = rooms[room];
            
            for (k in thisRoom.keys()) {
                var neighbor = thisRoom[k];
                if (neighbor != client && faces.exists(neighbor.id)) {
                    // send info about the new client's face to existing
                    // clients in the room
                    neighbor.sendMessage("/face " + client.name + " " + 
                                         faces[client.id].skin + "," + 
                                         faces[client.id].hair, 
                                         true);
                    // at the same time, send their facial info to the new 
                    // client
                    client.sendMessage("/face " + neighbor.name + " " +
                                       faces[neighbor.id].skin + "," +
                                       faces[neighbor.id].hair,
                                       true);
                }
            }
        }
    }
    
    /*
     * Remove a client from their current room, and destroy it if it's now empty.
     */
    function leaveRoom(client:ClientApi) {
        var room = client.room;
        
        broadcast(client, client.name + " has left.");
        
        if (room == null) return;
        
        if (rooms.exists(room)) {
            var thisRoom = rooms[room];
            
            if (thisRoom.exists(client.id)) {
                thisRoom.remove(client.id);
                client.room = null;
            }
            
            // check if room is now empty and should be destroyed
            for (k in thisRoom.keys()) {
                // there's at least one user in the room
                return;
            }
            
            rooms.remove(room);
        }
    }
    
    /*
     * Join a room, and create it if it doesn't already exist.
     */
    function joinRoom(client:ClientApi, room:String) {
        if (client.room == room) {
            client.sendMessage("You're already in [" + room + "].");
            return;
        }
        
        if (client.room != null) {
            leaveRoom(client);
        }
        
        if (!rooms.exists(room)) {
            rooms[room] = new Map();
        }
        
        // add this user to the room
        rooms[room][client.id] = client;
        client.room = room;
        broadcast(client, client.name + " has joined.");
        
        client.sendMessage("You are now in room [" + room + "].");
        
        // send names of other users in the room
        client.sendMessage("Users in this room:");
        
        for (k in rooms[room].keys()) {
            var neighbor = rooms[room][k];
            client.sendMessage("* " + neighbor.name + (neighbor == client ? " (you)" : ""));
        }
    }
    
    public static function main() {
        var args = Sys.args();
        var host = Defs.HOST;
        if (args.length > 0) host = args[0];
        if (args.length > 1) Defs.PORT = Std.parseInt(args[1]);
        
        var server = new Server();
        var port = Defs.PORT;
        trace("Starting server (HOST=" + host + ", PORT=" + port + ")");
        server.run(host, port);
    }
}
