package mochat;

import haxe.io.Bytes;
import haxe.Serializer;
import haxe.Unserializer;
import sys.net.Socket;
import mochat.Data;
import mochat.Defs;


enum ClientState {
    NEW;
    LOGIN;
    CHAT;
    AWAY;
}

class ClientApi {
    public var id:Guid;
    public var name:String;
    public var room:String;
    public var state:ClientState;
    var socket:Socket;
    
    public function new(s:Socket) {
        socket = s;
        id = Defs.guid();
        state = NEW;
    }
    
    /*
     * Send a message to the client
     */
    public function sendMessage(msg:String, official:Bool=true, newline=true) {
        if (official) msg = "# " + msg;
        Data.sendData(socket, msg, newline);
    }
    
    public function loginPrompt() {
        sendMessage("Welcome to mochat!");
        sendMessage("Login name:");
        state = LOGIN;
    }
    
    public function welcome() {
        sendMessage("Hi, " + name + "!");
        state = CHAT;
    }
    
    public function disconnect(server:Server) {
        server.stopClient(socket);
    }
}
