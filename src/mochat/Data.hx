package mochat;

import mochat.Defs;
#if server
import sys.net.Socket;
#else
import flash.net.Socket;
#end


class Data {
#if server
    public static function sendData(socket:Socket, data:String, newline=true) {
        socket.write(data + (newline ? "\n" : ""));
    }
    
    public static function readData(socket:Socket):String {
        return socket.read();
    }
#else
    public static function sendData(socket:Socket, data:String, newline=true) {
        socket.writeUTFBytes(data + (newline ? "\n" : ""));
    }
    
    public static function readData(socket:Socket):String {
        return socket.readUTFBytes(socket.bytesAvailable);
    }
#end
}
