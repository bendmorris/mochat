package mochat;

#if !server
import flash.geom.Point;
import com.haxepunk.graphics.Text;
#end


typedef Guid = Int;

class Defs {
    // server variables
    public static var HOST="54.244.244.60";
    public static var PORT=27278;
    
#if !server
    // client variables
    public static var RESOLUTIONS = [
        new Point(320, 200),
        new Point(640, 480),
        new Point(720, 480),
        new Point(800, 600),
        new Point(1024, 768),
        new Point(1280, 720),
        new Point(1920, 1080),
        new Point(2560, 1440),
    ];
    
#if !desktop
    public static var FPS=60;
    public static var WIDTH=800;
    public static var HEIGHT=600;
    public static var SCALE:Float=0.25;
#else
    public static var FPS=60;
    public static var WIDTH=1280;
    public static var HEIGHT=720;
    public static var SCALE:Float=0.25*720/600;
#end
    
    public static var FONT_OPTIONS:TextOptions={font:"font/quattrocento_sans.fnt", 
                                                size:16, color:0, wordWrap:true};
    public static inline var BGCOLOR=0xE0E0E0;
#end
    
    // the length, in bits, of a randomly generated ID
    static inline var ID_LEN=31;
    
    /*
     * Get a new random ID for a client with the specified number of bits.
     */
    public static function guid(bits:Int=ID_LEN):Guid {
        var id = Lambda.fold([for (i in 0 ... ID_LEN) Std.random(2)],
                              function(x, y) return y*2 + x, 0);
        return id;
    }
}
